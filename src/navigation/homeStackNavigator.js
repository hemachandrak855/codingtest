import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/homeScreen';
import HomeTabNavigator from './homeTabNavigator';

const Stack = createStackNavigator();

const HomeStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="users" component={HomeTabNavigator} options={{ title: 'user details' }} />
        </Stack.Navigator>
    );
}

export default HomeStackNavigator;