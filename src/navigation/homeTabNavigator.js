import * as React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ProfileTab from '../screens/userDetailsScreens/profileScreen';
import PostsTab from '../screens/userDetailsScreens/postsScreen';
import TodosTab from '../screens/userDetailsScreens/todosScreen';

const Tab = createBottomTabNavigator();

const HomeTabNavigator = () => {

    return (
        <Tab.Navigator
            tabBarOptions={{
                labelStyle: {
                    fontSize: 20
                },
                tabStyle: {
                    justifyContent: 'center',
                },
                style: {
                    height: 65
                }
            }}
        >
            <Tab.Screen name="Profile" component={ProfileTab} />
            <Tab.Screen name="Posts" component={PostsTab} />
            <Tab.Screen name="Todos" component={TodosTab} />
        </Tab.Navigator>
    );
}

export default HomeTabNavigator;