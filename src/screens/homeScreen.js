import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, Dimensions, Pressable } from 'react-native';
import MapView, { Marker, Geojson } from 'react-native-maps';
import AsyncStorage from '@react-native-async-storage/async-storage';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;


const HomeScreen = ({ navigation }) => {

    const [usersData, setUsersData] = useState([]);
    const [markers, setMarkers] = useState([]);

    useEffect(async () => {

        await fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => response.json())
            .then((json) => {
                // console.log('data: ', json)

                let markersData = [];

                json.forEach(element => {
                    let obj = element;
                    const latitude = obj.address.geo.lat;
                    const longitude = obj.address.geo.lng;
                    let newObj = {
                        "id": obj.id,
                        "name": obj.name,
                        "latlong": { latitude: Number(latitude), longitude: Number(longitude) }
                    }
                    // console.log('obj: ', newObj)
                    markersData.push(newObj)
                });

                setUsersData(json)
                setMarkers(markersData);
            })
            .catch((error) => {
                console.error(error);
            });
    }, []);

    const storeData = async (value) => {
        try {
            const jsonValue = JSON.stringify(value)
            await AsyncStorage.setItem('SELECTED_USER', jsonValue)
        } catch (e) {
            // saving error
        }
    }

    const markerClicked = (item) => {
        storeData(item)
        navigation.navigate('users',
            {
                screen: 'Profile',
                params: { data: item }
            }
        )
    }

    return (
        <View style={styles.container}>
            <MapView
                style={styles.mapStyle}
                showsUserLocation={true}
                zoomEnabled={true}
                zoomControlEnabled={true}
                initialRegion={{
                    latitude: 15.127474,
                    longitude: 78.503396,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01,
                }}
            >

                {markers.map((item, index) => (
                    <Marker
                        key={index}
                        coordinate={item.latlong}
                        title={item.name}
                        onPress={() => markerClicked(item)}
                    />

                ))}

                {/* <Marker
                    coordinate={{ latitude: 15.127474, longitude: 78.503396 }}
                    title={"Home"}
                    description={"My Home"}
                /> */}

            </MapView>


            {/* <Button
                title='go to userDetailsPage'
                onPress={() => navigation.navigate('users')}
            /> */}

        </View >
    )
};

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    mapStyle: {
        flex: 1
    }
})