import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Button, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const image = { uri: "https://images.unsplash.com/photo-1534353436294-0dbd4bdac845?ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" };

const ProfileTab = ({ route }) => {

    const { data } = route.params;
    const [address, setAddress] = React.useState("");
    const [postsCount, setPostsCount] = useState(0);
    const [todosCount, setTodosCount] = useState(0);
    const navigation = useNavigation();

    useEffect(() => {

        let userId = data.id;
        getUserDetails(userId);
        getPostFromServer(userId);
        getTodosFromServer(userId);
    }, []);

    const getUserDetails = async (id) => {
        let url = 'https://jsonplaceholder.typicode.com/users?id=' + id;

        await fetch(url)
            .then((response) => response.json())
            .then((json) => {

                let user = json[0];
                let fullAddress = concatinateAddress(user.address);
                setAddress(fullAddress);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    const getPostFromServer = async (id) => {
        let url = "https://jsonplaceholder.typicode.com/posts?userId=" + id;
        await fetch(url)
            .then((response) => response.json())
            .then((json) => {
                // console.log('postData: ', json)
                setPostsCount(json.length);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    const getTodosFromServer = async (id) => {
        let url = "https://jsonplaceholder.typicode.com/todos?userId=" + id;
        await fetch(url)
            .then((response) => response.json())
            .then((json) => {
                // console.log('postData: ', json)
                setTodosCount(json.length);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    const concatinateAddress = (address) => {

        let fullAddress = "";
        fullAddress += address.street || "";
        fullAddress += ", " + address.suite || "";
        fullAddress += ", " + address.city || "";
        fullAddress += ", " + address.zipcode || "";
        return fullAddress;
    }



    return (
        <View style={styles.container}>
            <ImageBackground
                source={image}
                style={styles.image} >
                <View style={styles.secondView}>
                    <View style={{ alignItems: 'center' }}>
                        <Image
                            style={{ height: 60, width: 60, borderRadius: 30 }}
                            source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcLHv3LkIGSqJi424XnQ6ZjDznaRXfqXZJVw&usqp=CAU' }}
                        />
                        <Text style={{ color: 'white', fontWeight: 'bold', marginVertical: 10 }}>{data.name || ""}</Text>
                        <Text style={{ color: 'white' }}>{address}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', width: 300, justifyContent: 'space-evenly', marginTop: 20 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Posts')}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'white' }}>{postsCount}</Text>
                                <Text style={{ color: 'white', backgroundColor: 'magenta', paddingHorizontal: 10 }}>Posts</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Todos')}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={{ color: 'white' }}>{todosCount}</Text>
                                <Text style={{ color: 'white', backgroundColor: 'magenta', paddingHorizontal: 10 }}>Todos</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
}

export default ProfileTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        flex: 0.5,
    },
    secondView: {
        flex: 1,
        backgroundColor: '#000000c0',
        justifyContent: 'center',
        alignItems: 'center'


    },
})