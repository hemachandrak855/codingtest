import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, FlatList, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const PostsTab = ({ route }) => {

    // const { data } = route.params;
    const [postData, setPostsData] = useState([]);
    const [user, setUser] = useState({});

    useEffect(() => {

        getData();
    }, [])

    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('SELECTED_USER');
            if (jsonValue) {
                let user = JSON.parse(jsonValue)
                console.log('user: ', user);
                setUser(user)
                getPostFromServer(user.id)
            }
        } catch (e) {
            // error reading value
        }
    }

    const getPostFromServer = async (id) => {
        let url = "https://jsonplaceholder.typicode.com/posts?userId=" + id;
        await fetch(url)
            .then((response) => response.json())
            .then((json) => {
                // console.log('postData: ', json)
                setPostsData(json);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    return (
        <View style={styles.container}>
            <Text style={{ backgroundColor: 'gray', height: 0.5 }}></Text>
            <FlatList
                data={postData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                    return (
                        <View style={styles.secondView}>
                            <Image
                                style={{ height: 60, width: 60, borderRadius: 30, marginRight: 10 }}
                                source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRcLHv3LkIGSqJi424XnQ6ZjDznaRXfqXZJVw&usqp=CAU' }}
                            />
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{user.name || ''}</Text>
                                <Text style={{ color: 'gray', marginBottom: 10 }}>14 May 4:45 pm</Text>
                                <Text style={{ fontSize: 10 }}>{item.body}</Text>
                            </View>
                        </View>
                    )
                }}
            />
        </View>
    );
}

export default PostsTab;

const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    secondView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginBottom: 5
    }
})