import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, FlatList } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ChildComp = ({ count, status, bgColor = "white" }) => {
    return (
        <View style={{ alignItems: 'center', backgroundColor: 'white', height: 80, justifyContent: 'space-evenly', paddingBottom: 10 }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'black' }}>{count}</Text>
            <Text style={{ fontSize: 10, fontWeight: '400', color: 'gray' }}>{status}</Text>
            <Text style={{ backgroundColor: bgColor, width: 15, height: 2 }}></Text>
        </View>
    )
}

const TodosTab = () => {

    const [todosData, setTodosData] = useState([]);
    const [user, setUser] = useState({});


    useEffect(() => {
        getData();
    }, [])

    const getData = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('SELECTED_USER');
            if (jsonValue) {
                let user = JSON.parse(jsonValue)
                console.log('user: ', user);
                setUser(user)
                getTodosFromServer(user.id)
            }
        } catch (e) {
            // error reading value
        }
    }

    const getTodosFromServer = async (id) => {
        let url = "https://jsonplaceholder.typicode.com/todos?userId=" + id;
        await fetch(url)
            .then((response) => response.json())
            .then((json) => {
                // console.log('postData: ', json)
                setTodosData(json);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    return (
        <View style={styles.container}>
            <Text style={{ backgroundColor: 'gray', height: 1 }}></Text>

            <View style={styles.secondView}>
                <ChildComp count={'45'} status={'OPEN'} bgColor={'red'} />
                <ChildComp count={'25'} status={'COMPLETED'} bgColor={'green'} />
                <ChildComp count={'4'} status={'OVERDUE'} bgColor={'gray'} />
                <ChildComp count={'19'} status={'POSITIVE'} bgColor={'orange'} />
            </View>
            <Text style={{ backgroundColor: 'gray', height: 0.5 }}></Text>


            <FlatList
                data={todosData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                    // console.log("todos: ", item.title)

                    let bgColor = item.completed ? 'green' : "red";

                    return (
                        <View>
                            <View style={styles.thirdView}>

                                <View style={{ height: 80, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ backgroundColor: bgColor, width: 5, height: 80 }}></View>
                                    <View style={{ width: '70%', marginLeft: 15 }}>
                                        <Text style={[styles.text2,]} numberOfLines={2}>{item.title} </Text>
                                        <Text style={styles.dateText}>{'03/08/2021'} </Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    {item.completed ? <Text style={[styles.text1, { backgroundColor: 'green' }]}>{'COMPLETED'}</Text> : <Text style={[styles.text1, { backgroundColor: 'red' }]}>{'OPEN'}</Text>}
                                </View>
                            </View>
                            <Text style={{ backgroundColor: 'gray', height: 0.5 }}></Text>
                        </View>
                    )
                }}
            />

        </View>
    );
}

export default TodosTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    secondView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        backgroundColor: 'white'
    },
    thirdView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    text1: {
        fontSize: 10,
        fontWeight: '600',
        color: 'white',
        padding: 10
    },
    text2: {
        fontSize: 16,
        fontWeight: '500',
    },
    dateText: {
        marginTop: 5,
        fontSize: 12,
        fontWeight: '400',
        color: 'gray'
    }
})